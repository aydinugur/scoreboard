package org.sportradar.scoreboard.aydinugur;

import org.junit.Assert;
import org.junit.Test;
import org.sportradar.scoreboard.aydinugur.Model.FootballGame;
import org.sportradar.scoreboard.aydinugur.Model.Game;

import java.util.List;

public class ScoreboardTest {

    @Test
    public void testAddMatchShouldGiveDifferentId() {
        Scoreboard<FootballGame> classUnderTest = new Scoreboard<>();

        FootballGame firstMatch = new FootballGame("Host1", "Guest1");
        FootballGame secondMatch = new FootballGame("Host2", "Guest2");

        long firstMatchId = classUnderTest.addMatch(firstMatch);
        long secondMatchId = classUnderTest.addMatch(secondMatch);

        Assert.assertNotEquals(firstMatchId, secondMatchId);
    }

    @Test
    public void testFinishFootballGame() {
        Scoreboard<FootballGame> classUnderTest = new Scoreboard<>();

        FootballGame match = new FootballGame("Host", "Guest");

        classUnderTest.addMatch(match);

        long matchId = match.getGameId();

        Assert.assertTrue(classUnderTest.finishGame(matchId));
        Assert.assertFalse(classUnderTest.finishGame(matchId));
    }

    @Test
    public void testIncrementScoreShouldGroup() {
        Scoreboard<FootballGame> classUnderTest = new Scoreboard<>();

        FootballGame match1 = new FootballGame("Host1", "Guest1");
        FootballGame match2 = new FootballGame("Host2", "Guest2");
        FootballGame match3 = new FootballGame("Host3", "Guest3");
        FootballGame match4 = new FootballGame("Host4", "Guest4");

        classUnderTest.addMatch(match1);
        classUnderTest.addMatch(match2);
        classUnderTest.addMatch(match3);
        classUnderTest.addMatch(match4);

        match4.incrementHostScore();
        match4.incrementHostScore();
        match4.incrementHostScore();

        List<Game> summary = classUnderTest.getSummary();

        Assert.assertEquals(summary.get(summary.size() - 1), match4);
    }

    @Test
    public void testIncrementScoreShouldGroupByCreatedOrder() {
        Scoreboard<FootballGame> classUnderTest = new Scoreboard<>();

        FootballGame match1 = new FootballGame("Host1", "Guest1");
        FootballGame match2 = new FootballGame("Host2", "Guest2");
        FootballGame match3 = new FootballGame("Host3", "Guest3");
        FootballGame match5 = new FootballGame("Host5", "Guest5");
        FootballGame match4 = new FootballGame("Host4", "Guest4");

        classUnderTest.addMatch(match1);
        classUnderTest.addMatch(match2);
        classUnderTest.addMatch(match3);
        classUnderTest.addMatch(match5);
        classUnderTest.addMatch(match4);

        match4.incrementHostScore();
        match4.incrementHostScore();
        match4.incrementHostScore();

        match5.incrementGuestScore();
        match5.incrementGuestScore();
        match5.incrementGuestScore();

        List<Game> summary = classUnderTest.getSummary();

        Assert.assertTrue(summary.indexOf(match5) < summary.indexOf(match4));
    }
}
