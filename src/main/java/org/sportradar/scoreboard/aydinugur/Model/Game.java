package org.sportradar.scoreboard.aydinugur.Model;

public abstract class Game implements Comparable<Game> {

    private long gameId;
    protected final String host;
    protected final String guest;
    protected ScoreNode scoreNode;

    public Game(String host, String guest) {
        this.host = host;
        this.guest = guest;
    }
    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public long getGameId() {
        return gameId;
    }

    public void setScoreNode(ScoreNode scoreNode) {
        this.scoreNode = scoreNode;
    }

    public abstract boolean finishGame();

    public abstract String getScore();

    @Override
    public int compareTo(Game game) {
        return this.gameId > game.gameId ? -1 : 1;
    }

    @Override
    public boolean equals(Object game) {
        if (game instanceof Game) {
            return ((Game) game).getGameId() == gameId;
        }

        return false;
    }
}
