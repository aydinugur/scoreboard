package org.sportradar.scoreboard.aydinugur.Model;

import java.util.Collections;
import java.util.PriorityQueue;

public class ScoreNode {
    private ScoreNode next;

    private final PriorityQueue<Game> matches;

    public ScoreNode() {
        this.matches = new PriorityQueue<>(Collections.reverseOrder());
        this.next = null;
    }

    public ScoreNode getNext() {
        return next;
    }

    public void addGame(Game game) {
        this.matches.add(game);
    }

    public void assignGameToNextNode(Game game) {
        this.matches.remove(game);

        if (this.next == null) {
            this.next = new ScoreNode();
        }

        game.setScoreNode(this.getNext());
        this.next.addGame(game);
    }

    public PriorityQueue<Game> getMatches() {
        return this.matches;
    }
}
