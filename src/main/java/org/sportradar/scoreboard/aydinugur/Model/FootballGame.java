package org.sportradar.scoreboard.aydinugur.Model;

import java.util.concurrent.atomic.AtomicInteger;

public class FootballGame extends Game {

    private final AtomicInteger hostScore;
    private final AtomicInteger guestScore;

    public FootballGame(String host, String guest) {
        super(host, guest);
        this.hostScore = new AtomicInteger();
        this.guestScore = new AtomicInteger();
    }

    @Override
    public String getScore() {
        return String.format("%s: %d - %s: %d", host, hostScore.get(), guest, guestScore.get());
    }

    @Override
    public boolean finishGame() {
        return this.scoreNode.getMatches().remove(this);
    }

    public int incrementHostScore() {

        if (!verifyScoreNodeExist()) {
            return -1;
        }

        this.scoreNode.assignGameToNextNode(this);

        return hostScore.incrementAndGet();
    }

    public int incrementGuestScore() {

        if (!verifyScoreNodeExist()) {
            return -1;
        }

        this.scoreNode.assignGameToNextNode(this);

        return guestScore.incrementAndGet();
    }

    private boolean verifyScoreNodeExist() {
        if (this.scoreNode == null) {
            System.err.println("The football game has not been assigned a scoreboard yet!");
            return false;
        }
        return true;
    }

    public int getHostScore() {
        return hostScore.get();
    }

    public int getGuestScore() {
        return guestScore.get();
    }
}
