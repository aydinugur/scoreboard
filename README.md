# Sportradar-Case

This project is intented for Sportradar hiring process.

# Scoreboard

**Description:** A scoreboard library of the Football World Cup. 

# Example usage

## To initialize:
```
Scoreboard<FootballGame> scoreboard = new Scoreboard<>();

FootballGame footballGame = new FootballGame("Host name", "Guest name");

scoreboard.addMatch(footballGame);

```

## To update score
```
footballGame.incrementHostScore(); // updates host score by 1
footballGame.incrementGuestScore(); // updates guest score by 1
```

## To finish a game
```
footballGame.finishGame();
```

## To get summary of live games
```
scoreboard.getSummary(); // returns a list of live games
```

Also, you can extend Game abstract class to use this library with your own games.