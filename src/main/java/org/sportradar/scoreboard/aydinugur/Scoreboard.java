package org.sportradar.scoreboard.aydinugur;

import org.sportradar.scoreboard.aydinugur.Model.FootballGame;
import org.sportradar.scoreboard.aydinugur.Model.Game;
import org.sportradar.scoreboard.aydinugur.Model.ScoreNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class Scoreboard<T extends Game> {

    private final AtomicLong id;
    private final Map<Long, T> matches;
    private final ScoreNode initialScoreNode;

    public Scoreboard() {
        id = new AtomicLong();
        matches = new ConcurrentHashMap<>();
        initialScoreNode = new ScoreNode();
    }

    public long addMatch(T game) {
        long gameId = id.getAndIncrement();

        game.setGameId(gameId);
        game.setScoreNode(this.initialScoreNode);
        initialScoreNode.addGame(game);
        matches.put(gameId, game);

        return gameId;
    }

    public boolean finishGame(long gameId) {
        if (!matches.containsKey(gameId)) {
            return false;
        }

        Game game = matches.get(gameId);
        matches.remove(gameId);

        return game.finishGame();
    }

    public List<Game> getSummary() {
        List<Game> summary = new LinkedList<>();

        ScoreNode temp = this.initialScoreNode;

        while (temp != null) {
            summary.addAll(temp.getMatches());
            temp = temp.getNext();
        }

        return summary;
    }
}
