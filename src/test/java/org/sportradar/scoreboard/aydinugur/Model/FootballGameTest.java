package org.sportradar.scoreboard.aydinugur.Model;

import org.junit.Assert;
import org.junit.Test;
import org.sportradar.scoreboard.aydinugur.Scoreboard;

public class FootballGameTest {

    @Test
    public void testFootballGameShouldStartWithZeroToZero() {
        FootballGame footballGame = new FootballGame("Host1", "Guest1");

        Assert.assertEquals(0, footballGame.getHostScore());
        Assert.assertEquals(0, footballGame.getGuestScore());
    }

    @Test
    public void testFootballGameIncrementScore() {
        Scoreboard<FootballGame> classUnderTest = new Scoreboard<>();

        FootballGame footballGame = new FootballGame("Host", "Guest");

        int initialHostScore = footballGame.getHostScore();
        int initialGuestScore = footballGame.getHostScore();

        classUnderTest.addMatch(footballGame);

        footballGame.incrementHostScore();
        footballGame.incrementGuestScore();

        Assert.assertEquals(initialHostScore + 1, footballGame.getHostScore());
        Assert.assertEquals(initialGuestScore + 1, footballGame.getGuestScore());
    }

}
